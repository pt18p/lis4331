> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile App Development

## Phuong Tran

### Assignment 3 Requirements:

*Four Parts*

1. Field to enter U.S. dollar amount: 1 – 100,000
2. Toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency
4. Sign for euros, pesos, and Canadian dollars
5. Add background color(s) or theme
6. Create and display launcher icon image
7. Create Splash/Loading Screen

#### README.md file should include the following items:

* Screenshot of running application's unpopulated user interface
* Screenshot of running application's toast notification
* Screenshot of running application's converted currency user interface

#### Assignment Screenshots:

*Screenshot of running Currency Converter App*:

![Image](img/image.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
