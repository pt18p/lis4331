> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile App Development

## Phuong Tran

### Assignment 2 Requirements:

*Four Parts*

1. Drop-down menu for total number of guests (including yourself): 1-10
2. Drop-down menu for tip percentange (5% increments): 0 - 25
3. Background color(s) or theme
4. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's unpopulated user interface
* Screenshot of running application's populated user interface


#### Assignment Screenshots:

*Screenshot of running Tip Calculator App*:

![App 1](img/1.png)

![App 2](img/2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
