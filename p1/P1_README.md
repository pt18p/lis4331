> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile App Development

## Phuong Tran

### Project 1 Requirements:

*Four Parts*

1. Splash screen image, app title, intro text
2. Artist's images and media
3. Images and buttons must be vertically and horizontally aligned
4. Add background color(s) or theme
5. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of running application's splash screen
* Screenshot of running application's follow-up screen
* Screenshot of running application's play and pause

#### Assignment Screenshots:

*Screenshot of running Classical Music App*:

![Image](img/img.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
