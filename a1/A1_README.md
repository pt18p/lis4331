> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile App Development

## Phuong Tran

### Assignment 1 Requirements:

*Four Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App
* git commands with short descriptions


> #### Git commands w/short descriptions:

1. git init - creates a new Git repository
2. git status - displays the state of the working directory and the staging area
3. git add - adds a change in the working directory to the staging area
4. git commit - captures a snapshot of the project's currently staged changes
5. git push - uploads local repository content to a remote repository
6. git pull - fetchs and downloads content from a remote repository and immediately updates the local repository to match that content
7. git branch - creates, lists, renames, and deletes branches.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![java Hello Screenshot](img/1.png)

*Screenshot of Android Studio - My First App*:

![Android Studio My First App Screenshot](img/2.png)

*Screenshots of running Android Studio - Contacts App*

![Contact App 1](img/3.png)

![Contact App 2](img/4.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
