> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile Apps Development

## Phuong Tran

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/A1_README.md "My A1 README.md file")
    - Install JDK 
    - INstall Android Studio
    - Create My First App and Contacts App
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions
    

2. [A2 README.md](a2/A2_README.md "My A2 README.md file")
    - Create drop-down menu for total number of guests 
    - Create drop-down menu for tip percentange 
    - Change background color(s) or theme
    - Create and display launcher icon image

3. [A3 README.md](a3/A3_README.md "My A3 README.md file")
    - Currency converter application
    - Create a splash screen
    - Create radio buttons for currency choices 
    - Create and display launcher icon image

4. [P1 README.md](p1/P1_README.md "My P1 README.md file")
    - Music playing app
    - Create a splash screen
    - Play and pause music
    - Create and display launcher icon image






